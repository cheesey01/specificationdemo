﻿namespace Domain.Exceptions
{
    using System;

    public class NoSearchCriteriaException : Exception
    {
    }
}
