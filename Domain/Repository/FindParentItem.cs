﻿namespace Domain.Repository
{
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Interfaces;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    using LinqKit;

    public class FindParentItem : IFind
    {
        private readonly IQueryable<ParentItem> parentItems;

        public FindParentItem(IEnumerable<ParentItem> clients)
        {
            this.parentItems = clients.AsQueryable();
        }

        public IQueryable<ParentItem> Find(ISpecification<ParentItem> specification)
        {
            // this is how to query EF (you do not need the AsQeryable)
            return this.parentItems.AsExpandable().AsQueryable().Where(specification.IsSatisfiedBy());
        }
    }
}
