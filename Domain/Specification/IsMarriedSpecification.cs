﻿namespace Domain.Specification
{
    using System;
    using System.Linq.Expressions;

    using Domain.Attributes;
    using Domain.Search;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    [SearchPropertyInfo("IsMarried", typeof(ParentItemSearch))]
    public class IsMarriedSpecification : ISpecification<ParentItem>, IParentItemSearchSpecification
    {
        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            return ex => ex.IsMarried;
        }
    }
}