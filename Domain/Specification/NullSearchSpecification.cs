﻿namespace Domain.Specification
{
    using System;
    using System.Linq.Expressions;

    using Domain.SearchTarget;

    using EfSpecificationPattern;

    public class NullSearchSpecification : ISpecification<ParentItem>
    {
        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            return ex => false;
        }
    }
}
