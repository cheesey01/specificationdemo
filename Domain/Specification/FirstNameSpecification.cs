﻿namespace Domain.Specification
{
    using System;
    using System.Linq.Expressions;

    using Domain.Attributes;
    using Domain.Exceptions;
    using Domain.Interfaces;
    using Domain.Search;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    [SearchPropertyInfo("FirstName", typeof(ParentItemSearch))]
    public class FirstNameSpecification : ISpecification<ParentItem>, IParentItemSearchSpecification, ISearchVauleSpecification
    {
        private string search;

        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            if (string.IsNullOrEmpty(this.search))
            {
                throw new NoSearchCriteriaException();
            }

            return ex => ex.FirstName.ToLower().Contains(this.search.ToLower());
        }

        public void SearchValue(dynamic search)
        {
            this.search = search;
        }
    }
}