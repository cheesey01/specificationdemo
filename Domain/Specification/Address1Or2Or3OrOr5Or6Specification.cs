﻿namespace Domain.Specification
{
    using System;
    using System.Linq.Expressions;

    using Domain.Attributes;
    using Domain.Exceptions;
    using Domain.Interfaces;
    using Domain.Search;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    [SearchPropertyInfo("Address", typeof(ParentItemSearch))]
    public class Address1Or2Or3OrOr5Or6Specification : ISpecification<ParentItem>, IParentItemSearchSpecification, ISearchVauleSpecification
    {
        private readonly Adress1Specification adress1Specification;
        private readonly Adress2Specification adress2Specification;
        private readonly Adress3Specification adress3Specification;
        private readonly Adress4Specification adress4Specification;
        private readonly Adress5Specification adress5Specification;
        private readonly Adress6Specification adress6Specification;

        private string search;

        public Address1Or2Or3OrOr5Or6Specification()
        {
            this.adress1Specification = new Adress1Specification();
            this.adress2Specification = new Adress2Specification();
            this.adress3Specification = new Adress3Specification();
            this.adress4Specification = new Adress4Specification();
            this.adress5Specification = new Adress5Specification();
            this.adress6Specification = new Adress6Specification();
        }

        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            if (string.IsNullOrEmpty(this.search))
            {
                throw new NoSearchCriteriaException();
            }

            return
                this.adress1Specification
                    .Or(this.adress1Specification)
                    .Or(this.adress2Specification)
                    .Or(this.adress3Specification)
                    .Or(this.adress4Specification)
                    .Or(this.adress5Specification)
                    .Or(this.adress6Specification)
                    .IsSatisfiedBy();
        }

        public void SearchValue(dynamic search)
        {
            this.search = search;
            this.adress1Specification.SearchValue(this.search);
            this.adress2Specification.SearchValue(this.search);
            this.adress3Specification.SearchValue(this.search);
            this.adress4Specification.SearchValue(this.search);
            this.adress5Specification.SearchValue(this.search);
            this.adress6Specification.SearchValue(this.search);
        }
    }
}