﻿namespace Domain.Specification
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using Domain.Exceptions;
    using Domain.Interfaces;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    public class Adress2Specification : ISpecification<ParentItem>, ISearchVauleSpecification
    {
        private string search;

        public void SearchValue(dynamic search)
        {
            this.search = search;
        }

        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            if (string.IsNullOrEmpty(this.search))
            {
                throw new NoSearchCriteriaException();
            }

            return ex => ex.Addresses.Any(a => a.Address2.ToLower().Contains(this.search.ToLower()));
        }
    }
}
