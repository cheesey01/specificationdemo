﻿namespace Domain.Specification
{
    using System;
    using System.Linq.Expressions;

    using Domain.Attributes;
    using Domain.Exceptions;
    using Domain.Interfaces;
    using Domain.Search;
    using Domain.SearchTarget;

    using EfSpecificationPattern;

    [SearchPropertyInfo("Age", typeof(ParentItemSearch))]
    public class AgeSpecification : ISpecification<ParentItem>, IParentItemSearchSpecification, ISearchVauleSpecification
    {
        private int search;

        public Expression<Func<ParentItem, bool>> IsSatisfiedBy()
        {
            if (this.search == 0)
            {
                throw new NoSearchCriteriaException();
            }

            return ex => ex.Age == this.search;
        }

        public void SearchValue(dynamic search)
        {
            this.search = search;
        }
    }
}