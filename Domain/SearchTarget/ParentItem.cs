﻿namespace Domain.SearchTarget
{
    using System;
    using System.Collections.Generic;

    public class ParentItem
    {
        public ParentItem(Guid id, string firstName, string lastName, bool isMarried, int age)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Addresses = new List<Address>();
            this.IsMarried = isMarried;
            this.Age = age;
            this.Addresses = new List<Address>();
        }

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<Address> Addresses { get; set; }

        public bool IsMarried { get; set; }

        public int Age { get; set; }

        public void AddAddress(Address address)
        {
            this.Addresses.Add(address);
        }
    }
}
