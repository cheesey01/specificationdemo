namespace Domain.SearchTarget
{
    using System;

    public class Address
    {
        public Address(Guid id, ParentItem parent, string address1, string address2, string address3, string address4, string address5, string address6)
        {
            this.Id = id;
            this.ParentId = parent.Id;
            this.Address1 = address1;
            this.Address2 = address2;
            this.Address3 = address3;
            this.Address4 = address4;
            this.Address5 = address5;
            this.Address6 = address6;
        }

        public Guid Id { get; set; }

        public Guid ParentId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string Address5 { get; set; }

        public string Address6 { get; set; }
    }
}