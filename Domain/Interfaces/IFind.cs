﻿namespace Domain.Interfaces
{
    using System.Linq;

    using Domain.SearchTarget;

    using EfSpecificationPattern;

    public interface IFind
    {
        IQueryable<ParentItem> Find(ISpecification<ParentItem> specification);
    }
}
