namespace Domain.Factory
{
    using System.Collections;
    using System.Collections.Generic;

    using Autofac.Features.Indexed;

    using Domain.Attributes;
    using Domain.Interfaces;
    using Domain.Search;
    using Domain.SearchTarget;
    using Domain.Specification;

    using EfSpecificationPattern;

    public class SearchSpecificationFactory
    {
        private readonly IIndex<string, IParentItemSearchSpecification> index;

        public SearchSpecificationFactory(IIndex<string, IParentItemSearchSpecification> index)
        {
            this.index = index;
        }

        public ISpecification<ParentItem> Generate(ParentItemSearch parentItemSearch)
        {
            ISpecification<ParentItem> rootSpecification = null;

            foreach (var propertyInfo in parentItemSearch.GetType().GetProperties())
            {
                ISpecification<ParentItem> nodeSpecification = null;
                var propertyInfoItem = new SearchPropertyInfoAttribute(propertyInfo.Name, parentItemSearch.GetType()).PropertyInfoItem;

                if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(string)
                    && !string.IsNullOrEmpty(propertyInfo.GetValue(parentItemSearch, null).ToString()))
                {
                    nodeSpecification = (ISpecification<ParentItem>)this.index[propertyInfoItem];
                    ((ISearchVauleSpecification)nodeSpecification).SearchValue(propertyInfo.GetValue(parentItemSearch, null));
                }

                if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(int)
                    && (int)propertyInfo.GetValue(parentItemSearch, null) > 0)
                {
                    nodeSpecification = (ISpecification<ParentItem>)this.index[propertyInfoItem];
                    ((ISearchVauleSpecification)nodeSpecification).SearchValue(propertyInfo.GetValue(parentItemSearch, null));
                }

                if (propertyInfo.CanRead && propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(List<>)
                    && ((IList)propertyInfo.GetValue(parentItemSearch, null)).Count > 0)
                {
                    nodeSpecification = (ISpecification<ParentItem>)this.index[propertyInfoItem];
                    ((ISearchVauleSpecification)nodeSpecification).SearchValue(propertyInfo.GetValue(parentItemSearch, null));
                }

                if (propertyInfo.CanRead && propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>)
                    && ((IDictionary)propertyInfo.GetValue(parentItemSearch, null)).Keys.Count > 0)
                {
                    nodeSpecification = (ISpecification<ParentItem>)this.index[propertyInfoItem];
                    ((ISearchVauleSpecification)nodeSpecification).SearchValue(propertyInfo.GetValue(parentItemSearch, null));
                }

                if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(bool)
                    && (bool)propertyInfo.GetValue(parentItemSearch, null))
                {
                    nodeSpecification = (ISpecification<ParentItem>)this.index[propertyInfoItem];
                }

                if (nodeSpecification != null)
                {
                    rootSpecification = rootSpecification == null
                                            ? nodeSpecification
                                            : rootSpecification.And(nodeSpecification);
                }
            }

            return rootSpecification ?? (rootSpecification = new NullSearchSpecification());
        }
    }
}