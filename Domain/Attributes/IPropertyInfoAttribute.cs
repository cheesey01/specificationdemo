﻿namespace Domain.Attributes
{
    public interface IPropertyInfoAttribute
    {
        string PropertyInfoItem { get; }
    }
}
