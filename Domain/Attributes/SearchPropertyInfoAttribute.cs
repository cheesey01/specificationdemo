﻿namespace Domain.Attributes
{
    using System;
    using System.Linq;

    public class SearchPropertyInfoAttribute : Attribute, IPropertyInfoAttribute
    {
        public SearchPropertyInfoAttribute(string propertyInfoName, Type type)
        {
            this.PropertyInfoItem = $"{type.FullName}.{propertyInfoName}";
        }

        public string PropertyInfoItem { get; private set; }

        public static string GetAttributeType<T>(Type type)
            where T : class
        {
            var att = (IPropertyInfoAttribute)type.GetCustomAttributes(true).OfType<T>().FirstOrDefault();
            if (att == null)
            {
                throw new Exception("Someone forgot to put the TypeEnumAttribute on an Interface!");
            }

            return att.PropertyInfoItem;
        }
    }
}