﻿namespace Domain.Search
{
    public class ParentItemSearch
    {
        public ParentItemSearch(string address, string firstName, string lastName, int age, bool isMarried)
        {
            this.Address = address;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Age = age;
            this.IsMarried = isMarried;
        }

        public string Address { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public int Age { get; private set; }

        public bool IsMarried { get; private set; }
    }
}
