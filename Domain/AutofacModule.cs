﻿namespace Domain
{
    using System.Reflection;

    using Autofac;

    using Domain.Attributes;
    using Domain.Factory;
    using Domain.Specification;

    using Module = Autofac.Module;

    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AssignableTo<IParentItemSearchSpecification>().Keyed<IParentItemSearchSpecification>(SearchPropertyInfoAttribute.GetAttributeType<SearchPropertyInfoAttribute>);
            builder.RegisterType<SearchSpecificationFactory>().InstancePerLifetimeScope();
        }
    }
}
