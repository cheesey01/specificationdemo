﻿namespace DemoTest
{
    using System;
    using System.Collections.Generic;

    using Domain.SearchTarget;

    public static class FakeParentItemData
    {
        public static IEnumerable<ParentItem> GetParentItems()
        {
            var items = new List<ParentItem>();
            for (var i = 0; i < 10000; i++)
            {
                var partentItem = new ParentItem(
                    Guid.NewGuid(),
                    FakeData.NameData.GetFirstName(),
                    FakeData.NameData.GetSurname(),
                    FakeData.BooleanData.GetBoolean(),
                    FakeData.NumberData.GetNumber(10, 70));

                for (var j = 0; j < 15; j++)
                {
                    partentItem.AddAddress(
                        new Address(
                            Guid.NewGuid(),
                            partentItem,
                            FakeData.PlaceData.GetAddress(),
                            FakeData.PlaceData.GetAddress(),
                            FakeData.PlaceData.GetAddress(),
                            FakeData.PlaceData.GetCity(),
                            FakeData.PlaceData.GetPostCode(),
                            FakeData.PlaceData.GetCountry()));
                }

                items.Add(partentItem);
            }

            return items;
        }
    }
}
