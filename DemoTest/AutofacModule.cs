﻿namespace DemoTest
{
    using System;

    using Autofac;

    using Module = Autofac.Module;

    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<Domain.AutofacModule>();
        }
    }
}
