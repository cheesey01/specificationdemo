﻿namespace DemoTest
{
    using System.Linq;

    using Autofac;

    using Domain.Factory;
    using Domain.Interfaces;
    using Domain.Repository;
    using Domain.Search;
    using Domain.Specification;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SearchTest
    {
        private SearchSpecificationFactory searchSpecificationFactory;

        private IFind repo;

        [TestInitialize]
        public void Setup()
        {
            AutofacLoader.Configure();
            this.repo = new FindParentItem(FakeParentItemData.GetParentItems());
            this.searchSpecificationFactory = AutofacLoader.Container.Resolve<SearchSpecificationFactory>();
        }

        [TestMethod]
        public void SearchTest1()
        {
            var parentItemSearch = new ParentItemSearch("London", string.Empty, string.Empty, 0, false);

            var search = this.searchSpecificationFactory.Generate(parentItemSearch);
            var sut = this.repo.Find(search);
            Assert.IsTrue(sut.Any());
            Assert.IsInstanceOfType(search, typeof(Address1Or2Or3OrOr5Or6Specification));
        }

        [TestMethod]
        public void SearchTest2()
        {
            var parentItemSearch = new ParentItemSearch("London", "John", string.Empty, 0, false);

            var search = this.searchSpecificationFactory.Generate(parentItemSearch);
            var sut = this.repo.Find(search);
            Assert.IsTrue(sut.Any());
        }
    }
}
