﻿namespace DemoTest
{
    using System.Data.Entity;

    using Autofac;

    public static class AutofacLoader
    {
        public static IContainer Container { get; set; }

        public static void Configure()
        {
            if (Container != null)
            {
                return;
            }

            var builder = new ContainerBuilder();

            builder.RegisterModule<AutofacModule>();
            Container = builder.Build();
        }
    }
}
