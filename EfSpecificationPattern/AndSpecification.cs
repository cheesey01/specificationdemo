﻿namespace EfSpecificationPattern
{
    using System;
    using System.Linq.Expressions;

    using LinqKit;

    internal class AndSpecification<T> : ISpecification<T>
    {
        private readonly ISpecification<T> left;
        private readonly ISpecification<T> right;

        public AndSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            this.left = left;
            this.right = right;
        }

        public Expression<Func<T, bool>> IsSatisfiedBy()
        {
            var leftExpression = this.left.IsSatisfiedBy();
            var rightExpression = this.right.IsSatisfiedBy();

            return leftExpression.And(rightExpression);
        }
    }
}
