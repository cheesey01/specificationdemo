﻿namespace EfSpecificationPattern
{
    using System;
    using System.Linq.Expressions;

    using LinqKit;

    internal class OrSpecification<T> : ISpecification<T>
    {
        private readonly ISpecification<T> left;
        private readonly ISpecification<T> right;

        public OrSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            this.left = left;
            this.right = right;
        }

        public Expression<Func<T, bool>> IsSatisfiedBy()
        {
            var leftExpression = this.left.IsSatisfiedBy();
            var rightExpression = this.right.IsSatisfiedBy();
            return leftExpression.Or(rightExpression);
        }
    }
}